export const ADD_PLAYER = 'player/ADD_PLAYER';
export const REMOVE_PLAYER = 'player/REMOVE_PLAYER';
export const INCREMENT_COUNTER = 'player/INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'player/DECREMENT_COUNTER';
export const SELECT_PLAYER = 'player/SELECT_PLAYER';