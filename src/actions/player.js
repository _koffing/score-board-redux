import * as PlayerActionTypes from '../actiontypes/player';

export const addPlayer = (name, id) => {
    return {
        type: PlayerActionTypes.ADD_PLAYER,
        name,
        id
    }
}

export const removePlayer = id => {
    return {
        type: PlayerActionTypes.REMOVE_PLAYER,
        id
    }
}

export const incrementCounter = id => {
    return {
        type: PlayerActionTypes.INCREMENT_COUNTER,
        id
    }
}

export const decrementCounter = id => {
    return {
        type: PlayerActionTypes.DECREMENT_COUNTER,
        id
    }
}

export const selectPlayer = id => {
    return {
        type: PlayerActionTypes.SELECT_PLAYER,
        id
    }
}