import * as PlayerActionTypes from '../actiontypes/player';

const initialState = {
    players: [
        {
            name: "Daniel",
            score: 33,
            id: 0,
            createdAt: "01/17/18",
            updatedAt: "01/17/18"
        },
        {
            name: "Matt",
            score: 24,
            id: 1,
            createdAt: "01/17/18",
            updatedAt: "01/17/18"
        },
        {
            name: "kelly",
            score: 34,
            id: 2,
            createdAt: "01/17/18",
            updatedAt: "01/17/18"
        },
        {
            name: "amanda",
            score: 55,
            id: 3,
            createdAt: "01/17/18",
            updatedAt: "01/17/18"
        }
    ],
    selectedPlayerId: -1
}

export default function Player(state=initialState, action) {

    var setScore = (scoredirection, id) => {
        var players = state.players.map((player) => {
            if (id === player.id) {
                let newScore = (scoredirection === 'increase') ? player.score + 1 : player.score - 1;
                return {
                    ...player,
                    score: newScore,
                    updatedAt: `${month}/${day}/${year}`
                }
            }
            return {
                ...player
            }
        });
        return {
            ...state,
            players: players
        }
    }

    var decrementCounter = (id) => {
        return setScore('decrease', id);
    }

    var incrementCounter = (id) => {
        return setScore('increase', id);
    }

    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    switch(action.type){
        case PlayerActionTypes.ADD_PLAYER:
        return {
            ...state,
            players: [
                ...state.players,
                {
                    name: action.name,
                    score: 0,
                    id: action.id,
                    createdAt: `${month}/${day}/${year}`
                }
            ]
        };

        case PlayerActionTypes.REMOVE_PLAYER:
            return {
                ...state,
                players: [
                    ...state.players.filter((player) => player.id !== action.id)
                ]
            }

        case PlayerActionTypes.INCREMENT_COUNTER:
            return incrementCounter(action.id);

        case PlayerActionTypes.DECREMENT_COUNTER:
            return decrementCounter(action.id);
        
        case PlayerActionTypes.SELECT_PLAYER:
            return {
                ...state,
                selectedPlayerId: action.id
            }
        default:
            return state;
    }
}
