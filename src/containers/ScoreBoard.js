import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../components/Header';
import Player from '../components/Player';
import AddPlayerForm from '../components/AddPlayerForm';
import PlayerDetails from '../components/PlayerDetails';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as PlayerActionCreators from '../actions/player';

class ScoreBoard extends Component {

  static propTypes = {
    title: PropTypes.string,
    players: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      score: PropTypes.number.isRequired,
      id: PropTypes.number.isRequired
    })).isRequired
  } 

  render() {
    const {dispatch, players} = this.props;
    // using bindActionCreators to wrap the PlayerActionCreators methods in a dispatch function
    const AddPlayer = bindActionCreators(PlayerActionCreators.addPlayer, dispatch);
    const removePlayer = bindActionCreators(PlayerActionCreators.removePlayer, dispatch);
    const incrementCounter = bindActionCreators(PlayerActionCreators.incrementCounter, dispatch);
    const decrementCounter = bindActionCreators(PlayerActionCreators.decrementCounter, dispatch);
    const selectPlayer = bindActionCreators(PlayerActionCreators.selectPlayer, dispatch);

    let selectedPlayer;
    if(this.props.selectedPlayerId !== -1){
      selectedPlayer = players.filter((player)=> player.id === this.props.selectedPlayerId);
    }

    const PlayerComponent = players.map((player, i) => {
      return <Player
        key={player.id}
        score={player.score}
        name={player.name}
        decrementCounter={()=> decrementCounter(player.id)}
        incrementCounter={()=> incrementCounter(player.id)}
        removePlayer={()=> removePlayer(player.id)}
        selectPlayer={()=> selectPlayer(player.id)}
      />;
    })
    console.log(this.props.selectedPlayerId);
    return ( 
      <div className="scoreboard">
        <Header 
        title={this.props.title}
        players={players}
        />
        <div className="players">
          {PlayerComponent}
        </div>
        <AddPlayerForm onAdd={AddPlayer}/>
        <div className="player-detail">
          <PlayerDetails selectedPlayer={selectedPlayer} selectedPlayerId={this.props.selectedPlayerId} />
        </div>
      </div>
    );
  }
}

//returns an object that gets merged into the scorboards props
const mapStateToProps = state => (
  {
    players: state.players,
    selectedPlayerId: state.selectedPlayerId
  }
);

Player.defaultProps = {
  title: "Score Board"
}

// connect subscribes Scoreboard to any changes in the state (or redux store updates)
// when any changes occur the mapStateToProps function is called and changes are passed to score board props
export default connect(mapStateToProps)(ScoreBoard);
