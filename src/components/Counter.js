import React from 'react';
import PropTypes from 'prop-types';

function Counter(props){
    return (
        <div className="counter">
            <button className="counter-action decrement" onClick={props.decrementCounter} >-</button>
            <div className="counter-score">{props.score}</div>
            <button className="counter-action increment" onClick={props.incrementCounter} >+</button>
        </div>
    );
}

Counter.propTypes = {
    score: PropTypes.number.isRequired,
    decrementCounter: PropTypes.func.isRequired,
    incrementCounter: PropTypes.func.isRequired
}

export default Counter;