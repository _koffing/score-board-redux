import React from 'react';
import PropTypes from 'prop-types';

const PlayerDetails = (props) => {
    console.log(props);
    if(props.selectedPlayer){
        return (
            <div>
                <h3>{props.selectedPlayer[0].name}</h3>
                <ul>
                    <li>
                        <span>Score: </span>
                        {props.selectedPlayer[0].score}
                    </li>
                    <li>
                        <span>Created: </span>
                        {props.selectedPlayer[0].createdAt}
                    </li>
                    <li>
                        <span>Updated: </span>
                        {props.selectedPlayer[0].updatedAt}
                    </li>
                </ul>
            </div>
        );
    } else{
        return (<p>Click on a player to view details</p>);
    }
}

PlayerDetails.propTypes = {
    selectedPlayerId: PropTypes.number.isRequired,
    selectedPlayer: PropTypes.object
}

export default PlayerDetails;