import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AddPlayerForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: ""
        }
        this.lastId = 1000;
    }

    setId = () => {
        this.lastId++;
        return this.lastId;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setId();
        this.props.onAdd(this.state.name, this.lastId);
        this.setState({ name: '' });
    }

    onNameChange = (e) => {
        this.setState({name: e.target.value});
    }

    render = () => {
        return (
            <div className="add-player-form">
                <form onSubmit={this.handleSubmit}>
                    <input type='text' value={this.state.name} placeholder="Add Player" onChange={this.onNameChange} />
                    <input type='submit' value="add player" />
                </form>
            </div>
        );
    }
}

AddPlayerForm.propTypes = {
    onAdd: PropTypes.func.isRequired
}
