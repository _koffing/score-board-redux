import React, { Component } from 'react';

class StopWatch extends Component {
    constructor(props) {
        super(props);
        this.hasRun = false;
        this.state = {
            running: false,
            ellapsedTime: 0,
            previousTime: 0
        }
    }

    onTick = () => {
        var et = Math.floor((Date.now() - this.state.previousTime)/1000);
        this.setState({
            ellapsedTime: et,
        });
    }

    toggleTimer = () => {
        if(this.state.running){
            this.setState({
                running: false
            });
            clearInterval(this.timerInterval);
        }else {
            //checks if perviousTime has been set
            if (!this.hasRun){
                this.setState({
                    running: true,
                    previousTime: Date.now()
                });
                this.hasRun = true;
            }else {
                this.setState({
                    running: true,
                });
            }
            this.timerInterval = setInterval(() => {
                this.onTick();
            }, 1000);
            
        }
    }

    resetTimer = () => {
        this.setState({
            previousTime: Date.now()
        });
    }

    render = () => {
        return(
            <div className="stopwatch">
                <h2>stopwatch</h2>
                <div className="Stopwatch-time">{this.state.ellapsedTime}</div>
                <button onClick={this.toggleTimer}>{this.state.running ? "Stop" : "Start"}</button>
                <button onClick={this.resetTimer}>Reset</button>
            </div>
        );
    }
}

export default StopWatch;