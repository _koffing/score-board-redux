import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ScoreBoard from './containers/ScoreBoard';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import PlayerReducer from './reducers/player';

//creating the redux store
const store = createStore(
    PlayerReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    //Provider makes the store available to all container components in the application, it only needs to wrap the root component.
    <Provider store={store} >
        <ScoreBoard />
    </Provider>
    ,document.getElementById("root")
); 